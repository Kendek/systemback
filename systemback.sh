#!/bin/bash
#
# System backup and restore script for Debian-based distributions
#
# 2018-2024, Krisztián Kende <krisztiankende@gmail.com>
#
# This script can be used freely at your own risk!
#

################################################################################
# Configuration settings
################################################################################

# Storage directory for created restore points.
# If the value is empty then the current directory (see 'pwd' command) will be
# used.
# The default value is empty.
storage_dir=""

# User-definable additional storage slots.
# If enabled, the default slot 0 can be overwritten by the SBSLOT environment
# variable. The maximum width of the SBSLOT is 16 characters.
# Possible values are 'on' or 'off'.
# The default value is off.
definable_slots=off

# Maximum number of the temporary restore points in a storage slot.
# The oldest temporary restore points will be automatically removed when the new
# ones are created.
# Possible values are between 1 and 99, but greater than 20 is not recommended.
# The default value is 5.
max_rp_num=5

# File size limit in MiB. Affects only to the files in the user's home
# directories.
# If the file in the user's home directory is greater than this value, then the
# restore point will not include it.
# Possible values are between 0 and 1024. 0 means that there is no size limit.
# The default value is 8, which means 8 MiB.
max_size=8

# These items will be excluded from the restore points. Affects only to the
# files and dirs in the user's home directories.
# Each lines must begin with a dot, because the restore points do not include
# any other items.
# The default items are .cache/gvfs
#                       .gvfs
#                       .local/share/Trash/files/*
#                       .local/share/Trash/info/*
#                       .Xauthority
#                       .ICEauthority
exclude="\
.cache/gvfs
.gvfs
.local/share/Trash/files/*
.local/share/Trash/info/*
.Xauthority
.ICEauthority"

################################################################################
# End of configuration settings
################################################################################

vers=3.6 # 2024.12.02

################################################################################

init()
{
  # ANSI escape codes for terminal emulators
  [ -t 1 ] && {
      bold1="\e[1m"
      red1="\e[31m"
      norm1="\e[0m"
      clear1="\e[2J\e[1;1H\ec"
    } || {
      [[ $1 =~ r|m|d ]] && exit 1 # Need an interactive shell
      bold1= red1= norm1= clear1=
    }

  rndc="$(tr -dc a-zA-Z0-9 </dev/urandom 2>/dev/null | head -c 20)"
  [ ${#rndc} -eq 20 ] || exit 1
  sdir=

  [ -t 2 ] && {
      bold2="\e[0;1m"
      red2="\e[1;31m"
      norm2="\e[0m"
      clear2="\e[2J\e[1;1H\ec"

      # Write the error output to a buffer
      exec 2> >(
          buf=() stop=

          while IFS= read -r l
          do [ "$l" = ${rndc:12} ] && {
              [ ! "$stop" ] && stop=1 || {
                  [ ${#buf[@]} -eq 0 ] || {
                      printf " ${bold2}Press ENTER to see the error output, or any other key to exit.$norm2 "
                      read i
                      printf "\n\n"
                      [ "$i" = ENTER ] && printf "%s\n" "${buf[@]}" ""
                    }

                  exit
                }
            } || {
              printf "%s\n" "$l"
              [ "$stop" ] || buf+=("$l")
            }
          done
        )

      epid=$!
    } || bold2= red2= norm2= clear2= epid=

  # Catch SIGHUP, SIGINT and SIGTERM
  trap "[ "\$sdir" ] && sumount
        printf \"\n\n ${red2}Interrupted processing, the script is exiting now.$norm2\n\n\"
        exit 14" HUP INT TERM

  local req="$(which attr fuser rsync)"

  [ $? -eq 0 ] || \
    for t in /{attr_attr,fuser_psmisc,rsync_rsync}
    do [[ "$req" =~ ${t%_*} ]] || error 2 $LINENO ${t#*_}
    done

  [ $EUID -eq 0 ] || error 4 $LINENO # This script does not work without root permissions

  # Avoid multiple starting
  [[ $1 =~ s|l|z ]] || {
      exec 3>/run/systemback.sh.lock
      flock -n 3 || error 3 $LINENO
      trap "rm -f /run/systemback.sh.lock" EXIT
    }

  [ "$1" = m ] && ! grep -q " /mnt " /proc/mounts && error 5 $LINENO # Allow the system reparation only when a partition is mounted
  [[ $definable_slots =~ ^(on|off)$ ]] || error 6 $LINENO "${bold2}definable_slots=$red2$definable_slots"
  [ "${max_rp_num##*[!0-9]*}" ] && [ ${max_rp_num:0:1} -ne 0 ] && [ $max_rp_num -le 99 ] || error 6 $LINENO "${bold2}max_rp_num=$red2$max_rp_num"
  [ "${max_size##*[!0-9]*}" ] && [ ${#max_size} -eq 1 -o ${max_size:0:1} -ne 0 ] && [ $max_size -le 1024 ] || error 6 $LINENO "${bold2}max_size=$red2$max_size"

  [[ $1 =~ n|r ]] && {
      fuser /var/lib/{dpkg,apt/lists}/lock >/dev/null 2>&1 && error 7 $LINENO # Avoid interfere with Debian package managers
    }

  cd "$([ "$storage_dir" ] && printf %s "$storage_dir" || printf %s "$PWD")" || error 8 $LINENO

  [ "$1" = s ] || {
      [[ $1 =~ n|r|m ]] && {
          # Filesystem check
          local tfile=${rndc:4}

          touch $tfile && {
              chmod 1345 $tfile && chown 99:101 $tfile
              [ $(stat -c %a%u%g $tfile ; rm -f $tfile) -eq 134599101 ] && chattr -fi .
            } || error 9 $LINENO
        }

      RP=SB_???????????????????? # Restore points regex pattern
    }

  [ $definable_slots = on ] && [ "$SBSLOT" ] || SBSLOT=0
}

error()
{
  [ "$epid" ] && printf "${rndc:12}\n" # Stop collecting the error output
  [ "$sdir" ] && sumount
  printf "$clear2\n $red2"

  case $1 in
    2)
      printf "A required tool is missing from the system!\n Please install the '$3' package and try again."
      ;;
    3)
      printf "The systemback.sh is currently running!\n Please wait until it is exited, then try again."
      ;;
    4)
      printf "Permission denied!\n Please try again with 'root' user, instead of '$(whoami)'."
      ;;
    5)
      printf "A required filesystem is missing!\n Please mount the system partition(s) under the '/mnt' directory and try again."
      ;;
    6)
      printf "The following configuration setting is not correct!\n\n   $3"
      ;;
    7)
      printf "A Debian package manager is currently active!\n Please wait until it is closed, then try again."
      ;;
    8)
      printf "The storage directory is missing or incorrect!"
      ;;
    9)
      printf "Incompatible filesystem!\n Please try again with a different storage directory."
      ;;
    10)
      printf "Failed to mount the system partition(s)!"
      ;;
    11)
      printf "Failed to create a new restore point!"
      [ "$3" ] && printf "\n Please increase the value of 'max_rp_num' or manually remove a restore point."
      ;;
    12)
      printf "The selected restore point is missing, incompatible or ambiguous."
      ;;
    13)
      printf "Failed to complete the $3 process."
  esac

  printf "$norm2\n\n  Debug code: $vers-$(($2-$(wc -l <<< $exclude)+6))\n\n" # Print script version and line number

  # Print the collected error output
  [ "$epid" ] && printf "${rndc:12}\n" && sleep .05 && [ -d /proc/$epid ] && {
      [ "$(getkey)" ] || printf ENTER
      echo
      wait $epid
    }

  exit $1
} >&2

getkey()
{
  while read -n 1 -t .1 ; do : ; done # Flush input buffer
  IFS= read -srn 1 i
  printf "$i"
}

xattr()
{
  case $1 in
    s)
      setfattr -n user.systemback.sh[${SBSLOT:0:16}]:"${@:2}" && sync "${*:${#@}}"
      ;;
    g)
      local val

      val="$(getfattr --only-value -n user.systemback.sh[${SBSLOT:0:16}]:"${@:2}" 2>/dev/null)" && \
        case $2 in
          index)
            [ "${val##*[!0-9]*}" ] && [ ${#val} -eq 1 -o ${val:0:1} -ne 0 ] || return 1
            printf %s "${val:0:2}"
            ;;
          name)
            printf %s "${val:0:50}"
            ;;
          date\&time)
            printf %s "${val:0:19}"
            ;;
          *)
            printf %s "$val"
        esac
  esac
}

rplist()
{
  local idx=
  lst=() rlst=()

  for i in $RP
  do [ -d "$i" ] && idx="$(xattr g index "$i")" && {
      [ $idx -gt 0 ] && lst+=("$idx $([ "$1" = l ] && printf %s "$(xattr g name "$i") ($(xattr g date\&time "$i"))$([ "$(xattr g rmflag "$i")" = manual ] && printf " *")" || printf %s "$i")") || {
          [ "$1" ] || rlst+=("$i")
        }
    }
  done

  if [ ${#lst[@]} -eq 0 ]
  then [[ $1 =~ l|z ]] && {
      [ ${#lst[@]} -eq 0 ] && printf "\n ${bold1}There are no restore points in the following storage dir and slot:$norm1\n\n  %s\n\n" "$PWD @slot $SBSLOT"
      exit
    }
  else readarray -t lst < <(printf "%s\0" "${lst[@]}" | sort -gz$2 | xargs -0n1)
  fi
}

rpselect()
{
  [ "$1" ] || [[ $2 =~ e|k|z ]] && {
      [ "${1##*[!0-9]*}" ] && [ ${1:0:1} -ne 0 ] || error 12 $LINENO
    }

  local idx= lst=()
  rp=()

  for i in $RP
  do [ -d "$i" ] && idx="$(xattr g index "$i")" && [ $idx -gt 0 ] && {
      [ "$1" ] && [ $idx -eq $1 ] && rp=("$i") && break
      lst+=("$idx $i")
    }
  done

  [ "$1" ] && {
      [ "$rp" ] || error 12 $LINENO
    } || {
      [ ${#lst[@]} -eq 0 ] && error 12 $LINENO
      rp=("$(printf "%s\0" "${lst[@]}" | sort -gz$2 | xargs -0n1 2>/dev/null | head -n 1 | cut -d " " -f 2-)")
    }

  [[ $2 =~ e|k|z ]] || printf "$clear1\n ${bold1}Selected restore point:$norm1\n\n  %s\n" "${rp[1]=$(xattr g name "$rp")} ($(xattr g date\&time "$rp"))"
}

smount()
{
  sdir=/tmp/SB_${rndc:4}
  mkdir $sdir && \
  mount -B $1 $sdir || error 10 $LINENO
  rid=$(stat -fc %i $1)

  for d in boot{,/efi} home opt root srv usr{,/local} var
  do [ -d $1$d ] && [ "$(stat -fc %i $1$d)" != "$rid" ] && ! mount -B $1$d $sdir/$d && error 10 $LINENO
  done
}

sumount()
{
  for d in boot{/efi,} home opt root srv usr{/local,} var
  do [ -d $sdir/$d ] && [ "$(stat -fc %i $sdir/$d)" != "$rid" ] && {
      [ "$1" ] && sync -f $sdir/$d
      umount -l $sdir/$d
    }
  done

  [ "$1" ] && sync -f $sdir
  umount -l $sdir
  rmdir $sdir
  sdir=
}

remove()
{
  # Remove all restore points with index 0
  [ ${#rlst[@]} -gt 0 ] && {
      for r in "${rlst[@]}"
      do
        printf "\nAuto-removing '$(xattr g name "$r")'... "
        chattr -Rfi "$r"
        rm -r "$r" 2>/dev/null && sync -f . && printf done || printf ${red1}failed$norm1
      done

      printf "\n$1"
      rlst=()
    }
}

restore()
{
  rpselect "$2"
  printf "\n Checking restore point integrity... "
  warn=("\n The $([ $1 = / ] && printf restored || printf repaired) system may not be able to boot or work properly" ".\n\n Press 'I' to ignore this warning and continue, or any other key to exit.$norm1 ")

  [ $(sha256sum < <(find "$rp"/* -printf %f%y%m%U%G%s%TY%Tm%Td%TH%TM%TS ; stat -c %a%u%g "$rp") | cut -d " " -f 1) = "$(xattr g checksum "$rp")" ] && printf "OK\n" || {
      printf "${red1}Checksum mismatch!\n\n ${bold1}WARNING!$norm1 ${bold1}This restore point has been modified since it was created!$warn${warn[1]}"
      [[ ! $(getkey) =~ i|I ]] && printf "\n\n" && return || echo
    }

  [ -s $1etc/machine-id ] && [ -s "$rp"/etc/machine-id ] && [ "$(<$1etc/machine-id)" != "$(<"$rp"/etc/machine-id)" ] && {
      printf "\n ${red1}${bold1}WARNING!$norm1 ${bold1}This restore point was created on another system than the current\n target!$warn without\n additional manual steps${warn[1]}"
      [[ ! $(getkey) =~ i|I ]] && printf "\n\n" && return || echo
    }

  [ $1 = / ] && rtype=restoration || rtype=reparation
  printf "\n ${bold1}Press ENTER to start the system files $rtype, or any other key to skip.$norm1 "

  [ "$(getkey)" ] && echo || {
      printf "\n\n"
      smount $1

      # At system restoration, make mounted directories in /snap and /var removable if missing from the restore point
      [ $1 = / ] && {
          mpts="$(grep -e " /snap/" -e " /var/" /proc/mounts | cut -d " " -f 2 | tac)"

          [ "$mpts" ] && {
              while read -r l
              do [ -d "$rp$(printf "$l")" ] || umount -l "$(printf "$l")"
              done <<< $mpts
            }
        }

      for i in boot cdrom home lib{32,64,x32} media mnt opt snap srv initrd.img{,.old} vmlinuz{,.old}
      do [ -e "$rp"/$i ] || [ ! -e $sdir/$i ] || rm -r $sdir/$i || error 13 $LINENO $rtype
      done

      rsync -aAXhH --progress --delete --include=/{bin,boot,cdrom,dev,etc,home,lib,lib32,lib64,libx32,media,mnt,opt,proc,run,sbin,snap,srv,sys,tmp,usr,var,initrd.img,initrd.img.old,vmlinuz,vmlinuz.old} --exclude=/{*,etc/mtab,usr/local/bin/systemback.sh$([ -s $sdir/usr/local/bin/systemback.sh ] || printf _)} --exclude=/{home,media,mnt,root,run,tmp,var/cache/fontconfig,var/lib/udisks2,var/run,var/tmp}/* --exclude={$RP,lost+found} "$rp"/* $sdir && rsync -aAh --progress --exclude=/* "$rp"/ $sdir || error 13 $LINENO $rtype
    }

  printf "\n ${bold1}Press ENTER to start the user's configuration files $rtype, or any other\n key to $([ "$sdir" ] && printf skip || printf exit).$norm1 "

  [ "$(getkey)" ] && {
      [ "$sdir" ] && echo || true
    } || {
      . <(xattr g config "$rp") && [ "${max_size##*[!0-9]*}" ] && [ $max_size -le 1024 ] || error 13 $LINENO $rtype
      printf "\n\n"
      [ "$sdir" ] || smount $1

      [ -d $sdir/home ] || {
          [ -e $sdir/home -o -h $sdir/home ] && ! rm $sdir/home && error 13 $LINENO $rtype
          mkdir $sdir/home || error 13 $LINENO $rtype
        }

      excl=()

      while IFS= read -r l
      do [ "${l:0:1}" = . ] && excl+=("--exclude=/$l")
      done <<< $exclude

      [ $max_size -eq 0 ] && msize= || msize=--min-size=$((max_size*1024*1024+1)) # Bytes, $max_size + 1

      for d in "$rp"/root "$rp"/home/*
      do
        [ "${d: -2}" = "/*" ] && break # Skip if /home is empty
        usr=${d##*/}
        idir=${d:${#rp}:${#d}-${#rp}-${#usr}-1}
        [ -d $sdir$idir/$usr ] || rsync -dpgoAX "$rp"$idir/$usr $sdir$idir || error 13 $LINENO $rtype

        [ "$msize" ] && {
            rsync -rptgoDAXh --progress $msize "${excl[@]}" --include=/.* --exclude=/* --exclude={$RP,*~} --link-dest=../ $sdir$idir/$usr/ $sdir$idir/$usr/SB_${rndc:4} || error 13 $LINENO $rtype
          }

        rsync -aAXhH --progress --delete "${excl[@]}" --include=.* --exclude=/* --exclude={$RP,*~} "$rp"$idir/$usr/ $sdir$idir/$usr
        rv=$?

        [ "$msize" ] && {
            rsync -aAXhm --progress --link-dest=SB_${rndc:4}/ $sdir$idir/$usr/SB_${rndc:4}/ $sdir$idir/$usr
            rm -r $sdir$idir/$usr/SB_${rndc:4}
          }

        [ $rv -eq 0 ] || error 13 $LINENO $rtype
      done
    }

  [ "$sdir" ] && {
      printf "\nFlushing filesystem buffers... "
      sumount s
      printf "done\n\n ${bold1}The $rtype process is done.\n\n Press ENTER to restart the system, or any other key to exit.$norm1 "
      [ "$(getkey)" ] || reboot
    }

  printf "\n\n"
}

case $1 in
  -n|--new)
    init n
    rplist
    remove "\n"
    cnt=0

    for r in "${lst[@]}"
    do [ ${r%% *} -gt $max_rp_num ] || [ "$(xattr g rmflag "${r#* }")" != manual ] || [ $((++cnt)) -lt $max_rp_num ] || error 11 $LINENO .
    done

    smount /
    excl=() ldest=()

    while IFS= read -r l
    do [ "${l:0:1}" = . ] && excl+=("--exclude=/$l")
    done <<< $exclude

    for r in "${lst[@]}"
    do [ ${r%% *} -gt 0 ] && ldest+=("--link-dest=../../${r#* }") && [ ${#ldest[@]} -eq 20 ] && break # Rsync don't handle more than 20 --link-dest args
    done

    rp=SB_$rndc
    mkdir -p $rp/home || error 11 $LINENO

    for c in "index -v 0" "name -v $(n="${*:2}" n="${n//\\/}" ; [ "$n" ] && n="${n:0:50}" && printf %s "${n//[$'\n' ]/\\040}" || printf $rndc)" "date&time -v $(date +%Y-%m-%d\\040%H:%M:%S)" "config -v max_size=$max_size\012exclude=\"$(e="${exclude// /\\040}" ; printf %s "${e//$'\n'/\\012}")\"" "rmflag -v auto"
    do xattr s $c $rp || {
        rm -r $rp
        error 11 $LINENO
      }
    done

    [ $max_size -eq 0 ] && msize= || msize=--max-size=${max_size}M

    for d in $(grep :/home/ /etc/passwd | cut -d : -f 6)
    do [ -d $d ] && {
        ldest2=()

        for l in "${ldest[@]}"
        do [ -d "${l:18}$d" ] && ldest2+=("--link-dest=../${l:12}$d")
        done

        until rsync -aAXhH --progress $msize "${excl[@]}" --include=/.* --exclude=/* --exclude={$RP,*~} "${ldest2[@]}" $sdir$d/ $rp$d
        do [ $? = 24 ] && sleep .5 || error 11 $LINENO
        done
      }
    done

    pass=

    until [ "$pass" ] || {
        rsync -aAXhH --progress $msize "${excl[@]}" --include=/.* --exclude=/* --exclude={$RP,*~} "${ldest[@]/%//root}" $sdir/root/ $rp/root && pass=1
      } && rsync -aAXhH --progress --include=/{bin,boot,cdrom,dev,etc,home,lib,lib32,lib64,libx32,media,mnt,opt,proc,root,run,sbin,snap,srv,sys,tmp,usr,var,initrd.img,initrd.img.old,vmlinuz,vmlinuz.old} --exclude=/{*,etc/mtab,etc/*.dpkg-old} --exclude=/{home,media,mnt,root,run,tmp,var/cache/apt/archives/partial,var/cache/fontconfig,var/lib/udisks2,var/lib/ureadahead,var/run,var/tmp}/* --exclude=/var/cache/apt/{*.bin,*.bin.*,archives/*.deb} --exclude={$RP,lost+found,*~} "${ldest[@]/..\//}" $sdir/* $rp
    do [ $? = 24 ] && sleep .5 || error 11 $LINENO
    done

    rsync -aA --exclude=/* $sdir/ $rp # Xattr on '/' is reserved for internal usage
    sumount
    printf "\nSetting write protection... "
    cd $rp
    size=0 buf=() ln=

    # Set immutable bit on every directory
    while IFS= read -rd $'\0' d
    do
      buf+=("$d")
      size=$((size+${#d}+1))

      # Execute chattr if the arg buffer reach 256 x 1024 - arg count
      [ $size -ge 262144 ] && {
          ! chattr +i "${buf[@]}" && ln=$LINENO && break # Prevents inappropriate stdin ioctl
          size=0 buf=()
        }
    done < <(find * -type d -print0)

    [ ! "$ln" ] && chattr +i "${buf[@]}" || error 11 ${ln-$LINENO}
    cd ..
    printf "done\n\nCalculating checksum... "
    xattr s checksum -v "$(sha256sum < <(find $rp/* -printf %f%y%m%U%G%s%TY%Tm%Td%TH%TM%TS ; stat -c %a%u%g $rp) | cut -d " " -f 1)" $rp || error 11 $LINENO # The checksum based on various metadata values
    printf "done\n\nFlushing filesystem buffers... "
    sync -f .
    [ ${#lst[@]} -eq 0 ] || readarray -t lst < <(printf "%s\0" "${lst[@]}" | sort -rgz | xargs -0n1) # Avoid duplicated indexes

    for r in "${lst[@]}" "0 $rp"
    do
      idx=${r%% *}

      [ $idx -gt 0 ] && {
          if [ "$(xattr g rmflag "${r#* }")" = manual ]
          then [ $idx -gt $max_rp_num ] || {
              [ $idx -eq $max_rp_num ] && ((--max_rp_num))
             } && continue
          else [ $idx -ge $max_rp_num ] && rlst+=("${r#* }") idx=-1
          fi

          chattr -i "${r#* }"
        }

      xattr s index -v $((idx+1)) "${r#* }" || error 11 $LINENO
      chattr +i "${r#* }"
    done

    printf "done\n"
    remove
    printf "\n ${bold1}The restore point is successfully created.$norm1\n\n"
    ;;
  -r|--restore)
    init r
    restore / "$2"
    ;;
  -m|--repair)
    init m
    restore /mnt/ "$2"
    ;;
  -s|--storage)
    init s
    printf "\n ${bold1}%s" "$PWD"
    printf "$norm1 @slot ${bold1}%s$norm1\n\n" "$SBSLOT"
    ;;
  -l|--list)
    init l
    rplist l
    printf "\n ${bold1}Index$norm1 - Restore Point's Name (Creation Date & Time) [Keep]\n\n"
    readarray -t lst < <(printf "%s\0" "${lst[@]}" | sort -gz | xargs -0n1)
    idx=0

    for r in "${lst[@]}"
    do
      [ ${r%% *} -eq $idx ] && continue || idx=${r%% *} # Hide restore points with non-unique indexes
      printf " $([ ${#lst[@]} -ge 10 ] && [ "${r%% *}" -lt 10 ] && printf " ")$bold1${r%% *}$norm1 - "
      printf "%s\n" "${r#* }"
    done

    echo
    ;;
  -z|--size)
    init z

    case $2 in
      "")
        rplist z r
        printf "\nCalculating incremental size... "
        len=0 slst=()

        while read s
        do
          slst+=($s)
          [ ${#s} -ge $len ] && len=$((${#s}+1))
        done < <(du -bcs $(printf "%s\n" "${lst[@]#* }") | cut -f 1 | tac | numfmt --to=iec-i --suffix=B --round=nearest)

        printf "done\n"
        cnt=${#slst[@]}

        for s in "${slst[@]}"
        do
          printf "\n%$((len-${#s}))s$bold1$s$norm1 "
          [ $((--cnt)) = ${#lst[@]} ] && printf "%s\n" "$PWD @slot $SBSLOT" || printf %s "$(xattr g name "${lst[$cnt]#* }")"
        done

        printf "\n\n"
        ;;
      *)
        rpselect "$2" z
        printf "\nCalculating apparent size... "
        printf "done\n\n $bold1$(du -bs "$rp" | cut -f 1 | numfmt --to=iec-i --suffix=B --round=nearest)$norm1 %s\n\n" "$(xattr g name "$rp")"
    esac
    ;;
  -e|--rename)
    init e
    rpselect "$2" e
    chattr -i "$rp" && xattr s name -v "$(n="${*:3}" n="${n//\\/}" ; [ "$n" ] && n="${n:0:50}" && printf %s "${n//$'\n'/ }" || printf $rndc)" "$rp" || exit 1
    chattr +i "$rp"
    ;;
  -k|--keep)
    init k
    rpselect "$2" k
    chattr -i "$rp" && xattr s rmflag -v manual "$rp" || exit 1
    chattr +i "$rp"
    ;;
  -d|--remove)
    init d
    rpselect "$2" r
    printf "\n ${bold1}Press ENTER to remove this restore point, or any other key to exit.$norm1 "

    [ "$(getkey)" ] || {
        printf "\n\nRemoving '${rp[1]}'... "
        chattr -i "$rp" && xattr s index -v 0 "$rp" || exit 1
        rplist r
        cnt=0

        for r in "${lst[@]}"
        do [ ${r%% *} -gt $((++cnt)) ] && {
            chattr -i "${r#* }" && xattr s index -v $((${r%% *}-1)) "${r#* }" || exit 1
            chattr +i "${r#* }"
          }
        done

        chattr -Rfi "$rp"/*
        rm -r "$rp" 2>/dev/null && sync -f . && printf "done\n\n ${bold1}The restore point is successfully removed.$norm1" || printf ${red1}failed$norm1
      }

    printf "\n\n"
    ;;
  *)
    printf "\n ${bold1}System backup and restore script for Debian-based distributions v$vers by Kendek$norm1\n\n  Available options:\n\n   -n, --new [NAME] %9s create a new restore point\n   -s, --storage %12s print the current storage directory path and the\n %28s slot\n   -l, --list %15s list available restore points\n   -z, --size [INDEX] %7s calculate the incremental size of the storage slot\n %28s or the apparent size of a restore point\n   -r, --restore [INDEX] %4s perform a system and/or user's configuration files\n %28s restoration\n   -m, --repair [INDEX] %5s same as -r but the target (root) directory will be\n %28s the '/mnt' instead of the '/'\n   -e, --rename INDEX [NAME]  rename a restore point\n   -k, --keep INDEX %9s set the restore point to be manually removable\n %28s only\n   -d, --remove [INDEX] %5s manually remove a restore point\n\n"
esac
